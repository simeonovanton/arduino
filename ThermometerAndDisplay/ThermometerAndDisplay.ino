
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define NUMFLAKES     10 // Number of snowflakes in the animation example

#define LOGO_HEIGHT   16
#define LOGO_WIDTH    16
#include <CapacitiveSensor.h>

CapacitiveSensor   cs_4_8 = CapacitiveSensor(4,8); // 1M resistor between pins 4 & 8, pin 8 is sensor pin, add a wire and or foil

int ThermistorPin = 0;
int Vo;
float R1 = 10000;
float logR2, R2, T, Tc, Tf;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;

void setup() {
   pinMode(LED_BUILTIN, OUTPUT);
   Serial.begin(9600);
    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x64
    //Serial.println(F("SSD1306 allocation failed"));
    //for(;;); // Don't proceed, loop forever
  }
}

void loop() {
  long sensor1 =  cs_4_8.capacitiveSensor(50);
  if(sensor1 >= 1000)
   {
   digitalWrite(LED_BUILTIN,HIGH);
   Vo = analogRead(ThermistorPin);
   R2 = R1 * (1023.0 / (float)Vo - 1.0);
   logR2 = log(R2);
   T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
   Tc = T - 273.15 - 4; //-4 is temperature correction comparing to real(tube) thermometer
   Tf = (Tc * 9.0)/ 5.0 + 32.0; 

   Serial.print("Temperature: "); 
   Serial.print(Tf);
   Serial.print(" F; ");
   Serial.print(Tc);
   Serial.println(" C");
   DisplayDrawTemperature((int)Tc);

   //delay(500);
    //drawlineright();
    //testdrawcircle();
    //display.clearDisplay();
    //display.display();
   }
   else{
    digitalWrite(LED_BUILTIN,LOW);
    display.clearDisplay();
    display.display();
   }  
}

void DisplayDrawTemperature(int Tc) {
  display.clearDisplay();
  display.setTextSize(1);             
  display.setTextColor(WHITE);        // Draw white text
  display.setCursor(0,0);            
  display.print("TEMPERATURE");
  display.setTextSize(5); 
  display.setCursor(10,20);     
  display.print(Tc);
  display.setTextSize(2);
  //display.setTextColor(WHITE);
  display.setCursor(70,20);                     
  display.print(" C");
  display.display();
  delay(2000);
}
